# ERPNext Patches

This project collects issues and their fixes -- temporary or permanent.

Go to *Issues* to see whats in the pipeline.

Go to [Wiki](https://git.fairkom.net/hosting/frappe/apps/erpnext-patches/-/wikis/home) to find fixes and descriptions.

The directory `patches` contains patches for issues that are bigger and/or more complex.



## License

MIT
